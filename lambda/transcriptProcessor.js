const AWS = require('aws-sdk');


exports.handler = async (event) => {
    // TODO implement
    console.log(JSON.stringify(event, null, 2));
    await getS3object(event);


    const response = {
        statusCode: 200,
        body: JSON.stringify('Hello from Lambda!'),
    };
    return response;
};


async function getS3object(event) {
    const s3 = new AWS.S3();

    let srcBucket = event.Records[0].s3.bucket.name;
    let srcKey = event.Records[0].s3.object.key;

    console.log("Params: srcBucket: " + srcBucket + " srcKey: " + srcKey + "\n")

    await s3.getObject({
        Bucket: srcBucket,
        Key: srcKey,
    }).promise().then(async data => {
        //console.log(data);
        let fileData = data.Body.toString('utf-8');
        //console.log(fileData);
        fileData = JSON.parse(fileData);
        if (fileData['results'] && fileData['results']['transcripts'].length > 0) {
            // write the transcript to ddb
            let transcriptData = fileData['results']['transcripts'][0]['transcript'];
            let jobName = fileData['jobName'];

            //console.log(transcriptData);
            await writeTranscriptToDDB(jobName, transcriptData);
        }

    })
}


async function setSentimentAnalysistoDDB(empId,timestamp,sentimentReport){

    const documentClient = new AWS.DynamoDB.DocumentClient();


  let params = {
        Key: {
            'employeeId': empId,
            'timestamp': timestamp
        },
        TableName: 'audio-analyser-data',
        AttributeUpdates: {
            'sentimentAnalysis': {
                Value: sentimentReport
            }
        }
    };
    await documentClient.update(params).promise().then(data => {
        console.log('done');
        console.log(data);
    })

}

async function setEntityListtoDDB(empId, timestamp, entityList){
    
        const documentClient = new AWS.DynamoDB.DocumentClient();


  let params = {
        Key: {
            'employeeId': empId,
            'timestamp': timestamp
        },
        TableName: 'audio-analyser-data',
        AttributeUpdates: {
            'entityList': {
                Value: entityList
            }
        }
    };
    await documentClient.update(params).promise().then(data => {
        console.log('done');
        console.log(data);
    })
}

async function getTranscriptEntityList(empId,timestamp,transcript){
    
    var comprehend = new AWS.Comprehend();
    var params = {
        LanguageCode: "en", /* required */
        Text: transcript     /* required */
      };
      await comprehend.detectEntities(params).promise().then(async entityList =>{
          console.log(entityList);
          await setEntityListtoDDB(empId, timestamp, entityList);

      });

}



async function getTranscriptSentiment(empId, timestamp, transcript) {
    
    var comprehend = new AWS.Comprehend();
    var params = {
        LanguageCode: "en", /* required */
        Text: transcript     /* required */
      };
      await comprehend.detectSentiment(params).promise().then(async sentimentReport =>{
          console.log(sentimentReport);
          await setSentimentAnalysistoDDB(empId, timestamp, sentimentReport)
      });

}



async function writeTranscriptToDDB(jobName, transcriptData) {
    const documentClient = new AWS.DynamoDB.DocumentClient();

    let empId_timestamp = jobName.replace('.mp3', '');
    let empId = empId_timestamp.split('_')[0];
    let timestamp = parseInt(empId_timestamp.split('_')[1]);

    console.log(empId, timestamp, transcriptData);

    let params = {
        Key: {
            'employeeId': empId,
            'timestamp': timestamp
        },
        TableName: 'audio-analyser-data',
        AttributeUpdates: {
            'transcript': {
                Value: transcriptData
            }
        }
    };
    await documentClient.update(params).promise().then(data => {
        console.log('done');
        console.log(data);
    })
    
   await getTranscriptSentiment(empId, timestamp, transcriptData);
   
   await getTranscriptEntityList(empId, timestamp, transcriptData);
    
    
}
