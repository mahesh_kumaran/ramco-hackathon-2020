const AWS = require("aws-sdk");
const rekognition = new AWS.Rekognition();


async function getFaceEmotionalAnalysis(srcBucket, srcKey) {

    var params = {
        Image: { /* required */
            S3Object: {
                Bucket: srcBucket,
                Name: srcKey
            }
        },
        Attributes: [
            "ALL"
        ]
    };
    await rekognition.detectFaces(params).promise().then(async faceDetails => {
        console.log(faceDetails);
        await setEmotionalAnalysisInDDB(srcKey, faceDetails)
    });

}


async function setEmotionalAnalysisInDDB(imageId, eaReport) {


    const documentClient = new AWS.DynamoDB.DocumentClient();
    let params = {
        Key: {
            'imageId': imageId
        },
        TableName: 'image-analyser-data',
        AttributeUpdates: {
            'eaReport': {
                Value: eaReport
            }
        }
    };
    await documentClient.update(params).promise().then(data => {
        console.log('done');
        console.log(data);
    })
}


async function geteaReportFromDDB() {

    const documentClient = new AWS.DynamoDB.DocumentClient();
    let params = {
        TableName: "image-analyser-data"
    };

    return documentClient.scan(params).promise().then(resp => {
        console.log(resp);
        return resp['Items']
    });
}

async function getSignedUrl(s3Key) {
    var s3 = new AWS.S3();
    var params = {Bucket: 'ramcohackathonimages', Key: s3Key, Expires: 600};
    var url = s3.getSignedUrl('getObject', params);
    console.log('The URL is', url);
    return {'signurl' : url}
}

exports.handler = async (event) => {
    
    console.log(event, null, 2); 
   
    
    if (event["queryStringParameters"] && event["queryStringParameters"]["type"] == "getImages") {
        let imageData = await geteaReportFromDDB();
        const response = {
            statusCode: 200,
            body: JSON.stringify(imageData),
             headers : {
                    "Access-Control-Allow-Origin": "*"
                }
        };
        return response;

    } else if (event["Records"]) {
        let srcBucket = event.Records[0].s3.bucket.name;
        let srcKey = event.Records[0].s3.object.key;
        console.log(srcBucket, srcKey);
        await getFaceEmotionalAnalysis(srcBucket, srcKey);
    } else  if (event["queryStringParameters"] && event["queryStringParameters"]["type"] == "getSignUrl") {
        let s3Key = event['queryStringParameters']['s3Key'];
         let data  = await getSignedUrl(s3Key);
         const response = {
            statusCode: 200,
            body: JSON.stringify(data),
             headers : {
                    "Access-Control-Allow-Origin": "*"
                }
        };
        return response;
    
    }
    

};