const AWS = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient();



async function getAudioLists(employeeId) {
    let params = {
        TableName: "audio-analyser-data",
        KeyConditionExpression: "employeeId = :eId",
        ExpressionAttributeValues: {
            ":eId": employeeId
        }
    };
    return documentClient.query(params).promise().then(resp => {
        console.log(resp);
    });

}


async function scanAudioList() {
     let params = {
        TableName: "audio-analyser-data"
    };
    return documentClient.scan(params).promise().then(resp => {
        console.log(resp);
        return resp['Items']
    });

}

async function sendAudioToTranscript(s3Key) {
    const transcribeService = new AWS.TranscribeService();
    let params = {
        "TranscriptionJobName": s3Key.split('/')[1],
        "LanguageCode": "en-US",
        "MediaSampleRateHertz": 44100,
        "MediaFormat": "mp3",
        "Media": {
            "MediaFileUri": "s3://ramcohackathon/" + s3Key
        },
        "OutputBucketName": 'ramcoaudiotranscript'
    };

    await transcribeService.startTranscriptionJob(params).promise()
        .then(data => {
            console.log(data);
        }).catch(err => console.log(err));

    console.log('transcript done');

}


async function writeAudioKey(s3Key) {


    const documentClient = new AWS.DynamoDB.DocumentClient();

    let empId_timestamp = s3Key.replace('audios/', '');
    empId_timestamp = empId_timestamp.replace('.mp3','');

    let empId = empId_timestamp.split('_')[0];
    let timestamp = empId_timestamp.split('_')[1];

    console.log(empId, timestamp, s3Key);

    let params = {
        Key: {
            'employeeId': empId,
            'timestamp': parseInt(timestamp)
        },
        TableName: 'audio-analyser-data',
        AttributeUpdates: {
            'audioKey': {
                Value: s3Key
            }
        }
    };
    await documentClient.update(params).promise().then(data => {
        console.log('done');
        console.log(data);
    })

}

exports.handler = async (event) => {

    console.log(JSON.stringify(event, null, 2));
  
    if (event["queryStringParameters"] && event["queryStringParameters"]["type"] == "getTranscripts") {

        if (event["queryStringParameters"]["employeeId"]) {
            let transcripts = await getAudioLists();
            const response = {
                statusCode: 200,
                body: transcripts
            };
            return response;
        } else {
            let scandata = await scanAudioList();
            const response = {
                statusCode: 200,
                body: JSON.stringify(scandata),
                headers : {
                    "Access-Control-Allow-Origin": "*"
                }
            };
            return response
        }


    } else if (event["Records"]) {
        
        var s3Key =  event["Records"][0]["s3"]["object"]["key"];
        await writeAudioKey(s3Key)
        await sendAudioToTranscript(s3Key)
    }

};

