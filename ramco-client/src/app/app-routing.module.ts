import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OneComponent } from './one/one.component';
import { MeetingComponent } from './meeting/meeting.component';
import { FunComponent } from './fun/fun.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  { path: 'one', component: OneComponent },
  { path: 'meeting', component: MeetingComponent },
  { path: 'fun', component: FunComponent },
  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
