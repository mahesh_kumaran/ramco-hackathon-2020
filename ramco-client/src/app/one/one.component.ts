import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-one',
  templateUrl: './one.component.html',
  styleUrls: ['./one.component.scss']
})
export class OneComponent implements OnInit {

  apiUrl = 'https://ljswzhe93i.execute-api.us-east-1.amazonaws.com/Dev/';
  employees = [
    {
      name: "John",
      role: "Developer",
      description: "This is a test decription",
      id: "a610075",
      imgUrl : 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxxGTM_qCY32GKrf1hUDyb5BLrnJElqDPtg5Ym_-5HM3blRnyf'
    },
    {
      name: "Lisa",
      role: "HR",
      description: "This is a test decription",
      id: "a610076",
      imgUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTZtAWc98QGKAghhBif7kEzELbl7j1sSdcnSHqziBOJCuwMmAD8'
    },
    {
      name: "Dave",
      role: "Devops",
      description: "This is a test decription",
      id: "a610077",
      imgUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSFNeZSbF7EOVV-3c7_VGIZAPg6h2XlJiIG4fCAbKX63mp0bZT_'
    },
    {
      name: "Mona",
      role: "Sr Developer",
      description: "This is a test decription",
      id: "a610070",
      imgUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTI6D9KPMLRDF-5iiqfz7fpiczzbTqGKcvsMyQslzy5PJp9NOED'
    },
    {
      name: "Matthew",
      role: "Network",
      description: "This is a test decription",
      id: "a610041",
      imgUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ7ZfIRF-pXMcvpwTEwlMpVGDJ5JYWeGQdK0Ma8y-J6J21M4Xeu'
    }

  ]
  constructor(private _http: HttpClient, private _router: Router) { }

  ngOnInit() {
  }


  

  analyseEmployee(eId) {
    console.log(eId);
    this._router.navigate(['/fun', { eId: eId}]);
  }
}
