import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import * as Chart from 'chart.js'

@Component({
  selector: 'app-meeting',
  templateUrl: './meeting.component.html',
  styleUrls: ['./meeting.component.scss']
})
export class MeetingComponent implements OnInit {
  imageApiUrl = 'https://m0lwckasch.execute-api.us-east-1.amazonaws.com/Dev';
  imageData: any = [];
  preSignUrl = '';


  canvas: any;
  ctx: any;
  myChart: any;

  sentiData = [0,0,0,0,0,0,0,0];

  constructor(private _http: HttpClient) { }

  ngOnInit() {
  }

  getAnalysedImages() {
    this._http.get(this.imageApiUrl, { params: { type: 'getImages' } }).subscribe(data => {
      console.log(data);
      this.imageData = data;
      this.imageData.forEach(e => {
        let emotion = e['eaReport']['FaceDetails'][0]['Emotions'];
        emotion.forEach(emo => {
          if(emo['Type'] == 'ANGRY') {
            this.sentiData[0] += emo['Confidence'];
          } else if (emo['Type'] == 'CALM') {
            this.sentiData[1] += emo['Confidence'];

          }else if (emo['Type'] == 'DISGUSTED') {
            this.sentiData[2] += emo['Confidence'];
          }
          else if (emo['Type'] == 'SURPRISED') {
            this.sentiData[3] += emo['Confidence'];  
          }
          else if (emo['Type'] == 'CONFUSED') {
            this.sentiData[4] += emo['Confidence'];  
          }
          else if (emo['Type'] == 'FEAR') {
            this.sentiData[5] += emo['Confidence'];      
          }
          else if (emo['Type'] == 'SAD') {
            this.sentiData[6] += emo['Confidence'];     
          }else if (emo['Type'] == 'HAPPY') {
            this.sentiData[7] += emo['Confidence'];
          }
        })
      })
      
      this.showBar();
    })
  }

  getSignUrl(s3Key: string) {
    this._http.get(this.imageApiUrl, { params: { type: 'getSignUrl', s3Key: s3Key } }).subscribe(data => {
      console.log(data);
      this.preSignUrl = data['signurl'];
    })
  }

  showBar() {


    this.canvas = document.getElementById('myChart');
    this.ctx = this.canvas.getContext('2d');
    if (this.myChart) {
      this.myChart.destroy();
    }

    var densityData = {
      label: 'Percent of sentiment among group',
      data: this.sentiData,
      backgroundColor: 'rgba(0, 99, 132, 0.6)'
    };
    
    this.myChart = new Chart(this.ctx, {
      type: 'bar',
      data: {
        labels: ["ANGRY ", "CALM", "DISGUSTED ", "SURPRISED", "CONFUSED ", "FEAR", "SAD", "HAPPY"],
        datasets: [densityData]
      }
    });



  }

}
