import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import * as Chart from 'chart.js'



@Component({
  selector: 'app-fun',
  templateUrl: './fun.component.html',
  styleUrls: ['./fun.component.scss']
})
export class FunComponent implements OnInit {

  employeeData: any = [];
  apiUrl = 'https://ljswzhe93i.execute-api.us-east-1.amazonaws.com/Dev/';
  eId = 'NA';
  monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

  // for pie and transcript block
  title = 'angular8chartjs';
  canvas: any;
  ctx: any;
  myChart: any;
  transcript = 'NA';

  // for line chart
  linecanvas: any;
  linectx: any;
  mylineChart: any;
  positiveData:any = [];
  dateTrend: any = [];
 
  // for entites block
  entities:any = new Set();

  constructor(private route: ActivatedRoute, private _http: HttpClient) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(params);
      this.eId = params['eId'];
      this.getAudioTranscripts();
    });
  }

  // function to get all the audio transcript from dynamodb
  getAudioTranscripts() {
    console.log('getting audio transcript');
    this._http.get(this.apiUrl, { params: { type: 'getTranscripts' } }).subscribe((data: Array<any> )=> {
      console.log(data);
      this.employeeData = data.filter(e => e.employeeId === this.eId);
      this.drawLine();
      this.getEntities();
    })
  }

  viewData(eData) {
    console.log(eData);

    this.transcript = eData.transcript ? eData.transcript : 'NA';

    let sentiscore = [1, 1, 1, 1]
    if (eData['sentimentAnalysis']) {
      let analysis = eData['sentimentAnalysis'];

      sentiscore[0] = analysis['SentimentScore']['Positive'];
      sentiscore[1] = analysis['SentimentScore']['Negative'];
      sentiscore[2] = analysis['SentimentScore']['Neutral'];
      sentiscore[3] = analysis['SentimentScore']['Mixed'];

    }

    this.canvas = document.getElementById('myChart');
    this.ctx = this.canvas.getContext('2d');
    if (this.myChart) {
      this.myChart.destroy();
    }

    // console.log(sentiscore);
    this.myChart = new Chart(this.ctx, {
      type: 'pie',
      data: {
        labels: ["Positive", "Negative", "Neutral", "Mixed"],
        datasets: [{
          label: '# of sentiment',
          data: sentiscore,
          backgroundColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(43, 102, 235, 1)',
          ],
          borderWidth: 1
        }]
      },
      options: {
        responsive: false,
        display: true
      }
    });
  }

  drawLine() {
    this.employeeData.forEach(e => {
      if(e['sentimentAnalysis']) {
        this.positiveData.push(e['sentimentAnalysis']['SentimentScore']['Positive']);
        let dateVal = new Date(e.timestamp);
        this.dateTrend.push(dateVal.getDate() + '/' + this.monthNames[dateVal.getMonth()])
      }
    })


    this.linecanvas = document.getElementById('myLineChart');
    this.linectx = this.linecanvas.getContext('2d');
    if(this.mylineChart) {
      this.mylineChart.destroy();
    }

    this.mylineChart = new Chart(this.linectx, {
      // The type of chart we want to create
      type: 'line',
      data: {
        labels: this.dateTrend,
        datasets: [{
          label: 'Positive trend of meetings',
          backgroundColor: 'rgb(255, 99, 132)',
          borderColor: 'rgb(255, 99, 132)',
          data: this.positiveData
        }]
      },
      options: {
        responsive: false,
        display: true
      }
    });
  }

  getEntities() {
    this.employeeData.forEach(e => {
      if(e['entityList']) {
        for(let entity of e['entityList']['Entities']) {
          // console.log(entity);
          this.entities.add(entity.Text);
        }
      }
    });
  }



}
